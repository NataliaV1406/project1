<?php
session_start();
if (isset($_SESSION["data"])) {

	$data = $_SESSION["data"];
}


?>

<!DOCTYPE html>
<html lang="en">

<head>


  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
  <title>Sidebar template</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
  crossorigin="anonymous">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <link rel="stylesheet" href="./css/profile.css">



</head>

<body>
  <div class="page-wrapper chiller-theme toggled">
    <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
      <i class="fas fa-bars"></i>
    </a>
    <nav id="sidebar" class="sidebar-wrapper">
      <div class="sidebar-content">
        <div class="sidebar-brand">
          <a href="#">pro sidebar</a>
          <div id="close-sidebar">
            <i class="fas fa-times"></i>
          </div>
        </div>
        <div class="sidebar-header">
          <div class="user-pic">
            <img class="img-responsive img-rounded" src="<?= $data['photo'] ?>"
            alt="User picture">
          </div>
          <div class="user-info">
            <span class="user-name"><?=$data['name'] ?>
            <strong><?=$data['surname'] ?></strong>
          </span>
          <span class="user-role">Administrator</span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>
      <!-- sidebar-header  -->
      <div class="sidebar-search">
        <div>
          <div class="input-group">
            <input type="text" class="form-control search-menu" id="search_name" placeholder="Search...">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
            </div>
          </div>
        </div>
      </div>
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>General</span>
          </li>
          <li>
            <a href="profile.php">
              <i class="fa fa-tachometer-alt"></i>
              <span>Profile</span>
            </a>
          </li>
          <li>
            <a href="edit.php">
              <i class="fa fa-tachometer-alt"></i>
              <span>Settings</span>
            </a>
          </li>
          <li>
            <a href="password.php">
              <i class="fa fa-tachometer-alt"></i>
              <span>password</span>
            </a>
          </li>
          <li>
            <a href="photo.php">
              <i class="fa fa-tachometer-alt"></i>
              <span>Photoes</span>
            </a>
          </li>
          
         <li>
            <a href="friend.php">
              <i class="fa fa-tachometer-alt"></i>
              <span>Friends</span>
            </a>
          </li>
          <li>
            <a href="post.php">
              <i class="fa fa-tachometer-alt"></i>
              <span>post</span>
            </a>
          </li>
         
         



          

        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
      <a href="notification.php">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification"></span>
      </a>
      <a href="chat.php">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification"></span>
      </a>
      <a href="edit.php">
        <i class="fa fa-cog"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="login.php" class="logout">
          <i class="fa fa-power-off"></i>
      </a>
     
    </div>
  </nav>
  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">



