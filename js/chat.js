$(document).ready(function(){
	$('#action_menu_btn').click(function(){
		$('.action_menu').toggle();
	});


	$.ajax({
		type: "post",
		url: "server.php",
		data: {key: "friend_show"},
		success: function(r){
			r=JSON.parse(r)
			
			
			r.forEach(function(el, index){
				
				$(".chat-friends").append(`

					<li class="friend-item ${index==0?'active activefr':''}" data-id="${el.id}">
					<div class="d-flex bd-highlight">
					<div class="img_cont">
					<img src="${el.photo}" class="rounded-circle user_img">
					</div>
					<div class="user_info">
					<span>${el.name} ${el.surname}</span>
					</div>
					</div>
					</li>
					`)
			})
			getMessage().then(() => {
				document.querySelector(".msg_card_body").scrollTop=document.querySelector(".msg_card_body").scrollHeight
			})
			setInterval(getMessage,1000)
		}
		
	})

	$(document).on("click", '.friend-item',function(){
		$('.friend-item').removeClass('active activefr')
		$(this).addClass('active activefr')
		$(".chat_with").empty()
		$(".chat_with").append(`
			<div class="img_cont">
			<img src="${$(this).find('img').attr('src')}" class="rounded-circle user_img">
			<span class="online_icon"></span>
			</div>
			<div class="user_info">
			<span>Chat with ${$(this).find('.user_info').html()}</span>
			
			</div>
			
		`)
		getMessage().then(() => {
			document.querySelector(".msg_card_body").scrollTop=document.querySelector(".msg_card_body").scrollHeight
		})
	})

	function getMessage(){
		let id=$(".activefr").attr("data-id")
		return $.ajax({
			type: "post",
			url: "server.php",
			data: {id: id, key: "message_show"},
			success: function(r){
				r=JSON.parse(r)
				console.log(r)
				$(".message_from_friend").remove()
				$(".message_from_me").remove()
				r.forEach(function(el, index){
					if(el.social_id==id){
						
						
						$(".msg_card_body").append(`
							<div class="d-flex justify-content-start mb-4 message_from_friend">
							<div class="img_cont_msg">
							<img src="${el.social_photo}" class="rounded-circle user_img_msg">
							</div>
							<div class="msg_cotainer">
							${el.text}
							<span class="msg_time">${el.time}</span>
							</div>

							</div)`)
					}else{
						
						$(".msg_card_body").append(`
							<div class="d-flex justify-content-end mb-4 message_from_me">
							<div class="msg_cotainer_send">
							${el.text}
							<span class="msg_time_send">8:55 AM, Today</span>
							</div>
							<div class="img_cont_msg">
							<img src="${el.social_photo}" class="rounded-circle user_img_msg">
							</div></div>`)
					}
					
					
				})
			}

		})

	}

	$(".send_btn").on('click',  function(event) {
		let id = $(".activefr").attr("data-id")
		let message=$(".message").val()
		console.log(id, message)
		$.ajax({
			type: "post",
			url: "server.php",
			data: {id: id, message: message, key: "friend_message"},
			success: function(r){
				$(".message").val("")

			}

		})
	});


	$("#search_name_chat").on("input", function(){
		let text=$(this).val()
		if (text != ""){
			$.ajax({
				type: "post",
				url: "server.php",
				data: {text: text, key: "search"},
				success: function(r){
					r=JSON.parse(r)
					console.log(r)
					$(".friend-item").remove()
					r.forEach(function(el, index){
						$(".chat-friends").append(`

							<li class="friend-item ${index==0?'active activefr':''}" data-id="${el.id}">
							<div class="d-flex bd-highlight">
							<div class="img_cont">
							<img src="${el.photo}" class="rounded-circle user_img">
							</div>
							<div class="user_info">
							<span>${el.name} ${el.surname}</span>
							</div>
							</div>
							</li>
							`)
					})

				}
			})
		} else {
			$.ajax({
				type: "post",
				url: "server.php",
				data: {key: "friend_show"},
				success: function(r){
					r=JSON.parse(r)
					$(".friend-item").remove()
					r.forEach(function(el, index){
						$(".chat-friends").append(`

							<li class="friend-item ${index==0?'active activefr':''}" data-id="${el.id}">
							<div class="d-flex bd-highlight">
							<div class="img_cont">
							<img src="${el.photo}" class="rounded-circle user_img">
							</div>
							<div class="user_info">
							<span>${el.name} ${el.surname}</span>
							</div>
							</div>
							</li>
							`)
					})
				}

			})

		}
	})
});