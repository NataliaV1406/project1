$(document).ready(function(){
	

let id=$("#friend_id").val()
$.ajax({
		type: "post",
		url: "server.php",
		data: {id: id, key: "friendprofile_show"},
		success: function(r){
			r=JSON.parse(r)
			
			let x= $(`<div class="friend_items"></div>

				`)
			r.forEach(function(el, index){
				x.append(`
                 
                 
			 	<div class="friend_item">
			 	<div class="friend_image">	
				<img class="image_friend" width="200"  src="${el.photo}">
				</div>
				<div class="friend_text">
				<h1 class="text-info user">Name :${el.name} ${el.surname}</h1>
			    
				<h3>Age: ${el.age}</h2>
				<button data-id="${el.id}" class="btn btn-info friend_photo">photos</button>
                <button data-id="${el.id}" class="btn btn-info friend_friends">friends</button>

				</div>
                </div>

				
				`)
			})
			$(".container_friend").append(x)
		}
		
	})

$.ajax({
		type: "post",
		url: "server.php",
		data: {id: id, key: "friendprofile_post"},
		success: function(r){
			r=JSON.parse(r)
			
      
      let x= $(` <div class="post__items " >
          
          </div>
        `)
      r.forEach(function(el, index){
        x.append(`
          <div class="post__item ">
                 <h5>${el.title}</h3>
                 <h5>From:  ${el.name} ${el.surname}</h5>
                 <p>${el.date} </p>
                 <p>${el.post} </p>

		        <span class="postlike"data-toggle="modal" data-target="#post_like" data-id="${el.id}">
		            <img class="icon text-info m-2" src="https://img.icons8.com/ultraviolet/40/000000/filled-like.png"/>${el.like}
		        </span>
		        <button data-id="${el.id}" class="btn btn-info like_post post_btn">like</button>
		        <span class="postcomment" data-id="${el.id}" data-toggle="modal" data-target="#post_comment">
		            <img class="icon text-info m-2" src="https://img.icons8.com/ultraviolet/40/000000/comments.png"/>${el.comment}
		        </span>
		        <button data-id="${el.id}" class="btn btn-info comment_post post_btn">comment</button>
           </div>
            
        `)
      })
      $(".container_post").append(x)
    }
		
	})
$(document).on("click", ".friend_photo", function(){
	console.log("ok")
	let id=$(this).attr("data-id")
	$.ajax({
	type: "post",
		url: "server.php",
		data: {id: id, key: "friend_photo"},
	    success: function(r){
			r=JSON.parse(r)
			let x= $(` <div class="container">				
					</div>
				`)
			r.forEach(function(el, index){
				x.append(` <div class="photo__item">
                    <div class="photo__press">
					<img src="${el.photos}">
					</div>
					</div>`)
			})
			$("container_friend_photo").append(x)
		}
    })

})

$(document).on("click", ".friend_friends", function(){
	let id=$(this).attr("data-id")
	$.ajax({
	type: "post",
		url: "server.php",
		data: {id: id, key: "friend_friends"},
	    success: function(r){
			r=JSON.parse(r)
			let x= $(`<div class="friend_items"></div>
				`)
			r.forEach(function(el, index){
				x.append(`
                
			 	<div class="friend_item">
			 		<div class="friend_image">	
						<img class="image_friend" width="200"  src="${el.photo}">
					</div>
					<div class="friend_text">
						<h1 class="text-info">Name : <a href="friend-profile.php?id=${el.id}">${el.name} ${el.surname}</a></h1>
			    		<h3>Age: ${el.age}</h2>
				 	</div>
                </div>
				
				`)
			})
			$(".container_friend").append(x)
		}
    })

})
$(document).on("click", ".like_post", function(){
      let id=$(this).attr("data-id")
              $.ajax({
              type: "post",
              url: "server.php",
              data: { id: id, key: "like_post"},
              success: function(r){
              }
      	 })
  })

  $(document).on("click", ".comment_post", function(){
    let id=$(this).attr("data-id")
    $(this).parents(".post__item").find(".comment").remove()
     let x= $(`<div class="comment_min m" > </div>`)
         x.append(`   
      
    		<textarea class="comment_form form-control textarea__form bg-light p-2 col-lg-5  border " placeholder="Enter your comment" type="text" name="comment_text"></textarea>
    
            <button type="submit" class="btn btn-info send_comment" data-id="${id}" > Send  comment</button>
    `)
   		 $(this).parents(".post__item").append(x)
   
        
  })

 $(document).on("click", ".send_comment", function(){
      let id=$(this).attr("data-id")
      let comment= $(this).parent().find(".comment_form").val()
        if (comment !=""){
		      $.ajax({
		      type: "post",
		      url: "server.php",
		      data: {id: id, comment: comment, key: "send_comment"},
		      success: (r)=>{
		        $(this).parent(".comment_min").remove()
		         }
		     
		    })
		} else {
			$(this).parent(".comment_min").remove()
		}
})
})