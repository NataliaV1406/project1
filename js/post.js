$(document).ready(function(){



$.ajax({
  type: "post",
    url: "server.php",
    data: { key: "show_post"},
    success: function(r){
      r=JSON.parse(r)
      
      let x= $(` <div class="post__items " >
          
          </div>
        `)
      r.forEach(function(el, index){
        x.append(`
          <div class="post__item ">
                 <h5>${el.title}</h3>
                 <h5>From:  ${el.name} ${el.surname}</h5>
                 <p>${el.post} </p>
        
              <button data-id="${el.id}" class="btn btn-info delete_post post_btn">delete</button>
        
        </div>
        `)
      })
      $(".container_post").append(x)
    }

})

$(document).on("click", ".delete_post", function(){
     let id=$(this).attr("data-id")
     $.ajax({
      type: "post",
      url: "server.php",
      data: {id: id, key: "delete_post"},
      success: (r) => {
          $(this).parent().remove()
      }
    })
  })
})