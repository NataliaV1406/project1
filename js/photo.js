$(document).ready(function(){
	$.ajax({
		type: "post",
		url: "server.php",
		data: { key: "photo_show"},
		success: function(r){
			r=JSON.parse(r)
			
			let x= $(` <div class="container">
					
					</div>
				`)
			r.forEach(function(el, index){
				x.append(`
                    <div class="photo__item">
                    <div class="photo__press">
				<img src="${el.photos}">
				</div>
				<button data-id="${el.id}" class="btn btn-info delete_photo">delete photo</button>
				<button data-id="${el.id}" class="btn btn-info profile_photo">profile photo</button>
				</div>
				`)
			})
			$("body").append(x)
		}
		
	})
	$(document).on("click", ".delete_photo", function(){
		let id=$(".delete_photo").attr("data-id")
		$.ajax({
			type: "post",
			url: "server.php",
			data: {id: id, key: "delete_photo"},
			success: (r) => {
					$(this).parent().remove()
			}
		})
	})
    $(document).on("click", ".profile_photo", function(){
		let id=$(this).attr("data-id")
		$.ajax({
			type: "post",
			url: "server.php",
			data: {id: id, key: "profile_photo"},
			success: function(r){
               
			}
		})
	})
})
