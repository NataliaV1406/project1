$(document).ready(function(){
  
    
    $.ajax({
      type: "post",
      url: "server.php",
      data: { key: "notification"},
      success: function(r){
       r=JSON.parse(r)
       // console.log(r)
       let x= $(`<div ></div>`)
       r.forEach(function(el, index){
        x.append(`   
          <div class="div_notification_friend">    
          <h1>${el.name}  ${el.surname}</h1>
           <img class="img-thumbnail image" width="300px" src="${el.photo}">
          <button data-id="${el.id}" class="btn btn-success accept" name=" accept">accept</button> 
          <button data-id="${el.id}" class="btn btn-success delete_request" name=" delete_request">delete</button> 
          </div>	
          `)
      })
       $("body").append(x)
     }

   })

     $(document).on("click", ".accept", function(){
    
    let from_id=$(this).attr("data-id")
    $.ajax({
      type: "post",
      url: "server.php",
      data: {from_id: from_id, key: "accept"},
      success: (r) => {
           $(this).parent().remove()
      }

    })

  })

  $(document).on("click", ".delete_request", function(){
   
    let from_id=$(this).attr("data-id")
    
    $.ajax({
      type: "post",
      url: "server.php",
      data: {from_id: from_id, key: "delete_request"},
      success: (r)=>{

          $(this).parent().remove()
        

      }

    })

  })
   

  })
