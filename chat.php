
<?php include "header.php"; ?>

<link rel="stylesheet" href="./css/chat.css">


<div class="container-fluid h-100">
	<div class="row justify-content-center h-100">
		<div class="col-md-4 col-xl-4 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
			<div class="card-header">
				<div class="input-group">
					<input type="text" placeholder="Search..." id="search_name_chat" class="form-control search">
					<div class="input-group-prepend">
						<span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
					</div>
				</div>
			</div>
			<div class="card-body contacts_body">
				<ul class="contacts chat-friends">
					

				</ul>
			</div>
			<div class="card-footer"></div>
		</div></div>
		<div class="col-md-8 col-xl-8 chat">
			<div class="card">
				<div class="card-header msg_head">
					<div class="d-flex bd-highlight chat_with">
						
						
					</div>
					<span id="action_menu_btn"><i class="fas fa-ellipsis-v"></i></span>
					<div class="action_menu">
						<ul>
							<li><i class="fas fa-user-circle"></i> View profile</li>
							<li><i class="fas fa-users"></i> Add to close friends</li>
							<li><i class="fas fa-plus"></i> Add to group</li>
							<li><i class="fas fa-ban"></i> Block</li>
						</ul>
					</div>
				</div>
				<div class="card-body msg_card_body">
					<div class="d-flex justify-content-start mb-4 message_from_friend">
						
					</div>
					<div class="d-flex justify-content-end mb-4 message_from_me">
						
					</div>
					
				</div>
				<div class="card-footer">
					<div class="input-group">
						<div class="input-group-append">
							<span class="input-group-text attach_btn"><i class="fas fa-paperclip"></i></span>
						</div>
						<textarea name="message" class="form-control type_msg message" placeholder="Type your message..."></textarea>
						<div class="input-group-append">
							<span class="input-group-text send_btn"><i class="fas fa-location-arrow"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>







<?php include "footer.php"; ?>
<script src="./js/chat.js"></script>