<?php
session_start();
if (isset($_SESSION["errors_login"])) {

	$errors_login = $_SESSION["errors_login"];
}


session_destroy();

?>




<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="./css/login.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<title>login</title>
</head>

<body>


	<form  action="server.php" method="post" class="w-50 mx-auto p-3 border">
    	<?php if (isset($errors_login["email_error"])) {
			print "<label class='text-danger'>" . $errors_login["email_login"] . "</label>";
		} ?>
		E-mail:    <input type="text" class="form-control mb-2 emaillogin" name="email">
		<?php if (isset($errors_login["password_error"])) {
			print "<label class='text-danger'>" . $errors_login["password_login"] . "</label>";
		} ?>
		Password: <input type="text" class="form-control mb-2 passwordlogin" name="password">
		 <button type="submit" name="btn_login" class="btn btn-info sing__in">Log in</button>
	</form> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="./js/script.js"></script>
</body>

</html>