<?php
session_start();
if (isset($_SESSION["errors"])) {

	$errors = $_SESSION["errors"];
}


session_destroy();

?>


<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="./css/profile.css">

	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>project</title>
	<style >
		
	</style>
</head>

<body>
	
	<div class="card bg-light">
    <article class="card-body mx-auto" style="max-width: 400px;">
	<h4 class="card-title mt-3 text-center">Create Account</h4>
	<p class="text-center">Get started with your free account</p>
	<p>
		<a href="" class="btn btn-block btn-twitter"> <i class="fab fa-twitter"></i>   Login via Twitter</a>
		<a href="" class="btn btn-block btn-facebook"> <i class="fab fa-facebook-f"></i>   Login via facebook</a>
	</p>
	<p class="divider-text">
        <span class="bg-light">OR</span>
    </p>
	
			<div class="form-group input-group">
				<div class="input-group-prepend">
				    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
				 </div>
				 <?php if (isset($errors["name"])) {
			print "<label class='text-danger'>" . $errors["name"] . "</label>";
		         } ?>
		        <input class="form-control name" placeholder="Name" type="text" name="name">
		         <?php if (isset($errors["surname"])) {
			print "<label class='text-danger'>" . $errors["surname"] . "</label>";
		         } ?>
		        <input class="form-control surname" placeholder="Surname" type="text" name="surname">
		    </div> <!-- form-group// -->
		    <div class="form-group input-group">
		    	<div class="input-group-prepend">
				    <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
				 </div>
				  <?php if (isset($errors["email"])) {
			print "<label class='text-danger'>" . $errors["email"] . "</label>";
		         } ?>
		        <input  class="form-control email" placeholder="Email address" type="email" name="email">
		    </div> <!-- form-group// -->
		    <div class="form-group input-group">
		    	<div class="input-group-prepend">
				    <span class="input-group-text"> <i class="fa fa-user"></i> </span>
				 </div>
				 <?php if (isset($errors["age"])) {
			print "<label class='text-danger'>" . $errors["age"] . "</label>";
		         } ?>
		        <input class="form-control age" placeholder="Age" type="text" name="age">
		    </div> <!-- form-group// -->
		    <div class="form-group input-group">
		    	<div class="input-group-prepend">
				    <span class="input-group-text"> <i class="fa fa-building"></i> </span>
				</div>
				<?php if (isset($errors["gender"])) {
			print "<label class='text-danger'>" . $errors["gender"] . "</label>";
		         } ?>
				<select class="form-control gender">
					<option selected disabled>Select gender </option>
					<option value="male">Male</option>
					<option value="female">Female</option>
					
				</select>
			</div> <!-- form-group end.// -->
		    <div class="form-group input-group">
		    	<div class="input-group-prepend">
				    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
				</div>
				<?php if (isset($errors["email"])) {
			print "<label class='text-danger'>" . $errors["password"] . "</label>";
		         } ?>
		        <input class="form-control password" placeholder="Create password" type="password" name="password">
		    </div> <!-- form-group// -->
		    <div class="form-group input-group">
		    	<div class="input-group-prepend">
				    <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
				</div>
				<?php if (isset($errors["cpassword"])) {
			print "<label class='text-danger'>" . $errors["cpassword"] . "</label>";
		         } ?>
		        <input class="form-control cpassword" placeholder="Repeat password" type="password" name="cpassword">
		    </div> <!-- form-group// -->                                      
		    <div class="form-group">
		        <button type="submit" class="btn btn-primary btn-block save"> Create Account  </button>
		    </div> <!-- form-group// -->      
		    <p class="text-center">Have an account? <a href="login.php">Log In</a> </p>                                                                 
</article>
</div> <!-- card.// -->

</div> 
<!--container end.//-->



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="./js/script.js"></script>


<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>

</html>