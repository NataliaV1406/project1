<?php
session_start();
if (isset($_SESSION["change_errors"])) {

	$change_errors = $_SESSION["change_errors"];
}
unset($_SESSION["change_errors"]);
if (isset($_SESSION["data"])) {

	$data = $_SESSION["data"];
}


?>



<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>project</title>
</head>

<body>
	<form action="server.php" class="w-50 bg-light mx-auto p-3 border" method="post">
		<?php if (isset($change_errors["name"])) {
			print "<label class='text-danger'>" . $change_errors["name"] . "</label>";
		} ?>
		Name: <input type="text" class="form-control mb-2 emaillogin" name="name" value="<?= $data['name'] ?>">
		<?php if (isset($change_errors["surname"])) {
			print "<label class='text-danger'>" . $change_errors["surname"] . "</label>";
		} ?>
		Surname: <input type="text" class="form-control mb-2 emaillogin" name="surname" value="<?= $data['surname'] ?>">
		<?php if (isset($change_errors["age"])) {
			print "<label class='text-danger'>" . $change_errors["age"] . "</label>";
		} ?>
		Age: <input type="text" class="form-control mb-3 age" name="age" value="<?= $data['age'] ?>">

		<button class="btn btn-success savechanges" name="savechanges">Save changes</button>

	</form>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="./js/script.js"></script>
</body>

</html>