<?php
session_start();
if (isset($_SESSION["password_errors"])) {

	$password_errors = $_SESSION["password_errors"];
}
unset($_SESSION["change_errors"]);
if (isset($_SESSION["data"])) {

	$data = $_SESSION["data"];
}

?>




<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>profile</title>
</head>

<body>





	<form action="server.php" class="w-50 mx-auto p-3 border" method="post">
		<?php if (isset($password_errors["opassword"])) {
			print "<label class='text-danger'>" . $password_errors["opassword"] . "</label>";
		} ?>
		Old password: <input type="password" class="form-control mb-3 password" name="opassword">
		<?php if (isset($password_errors["npassword"])) {
			print "<label class='text-danger'>" . $password_errors["npassword"] . "</label>";
		} ?>
		New password: <input type="password" class="form-control mb-3 password" name="npassword">
		<?php if (isset($password_errors["cpassword"])) {
			print "<label class='text-danger'>" . $password_errors["cpassword"] . "</label>";
		} ?>
		Confirm new password : <input type="password" class="form-control mb-3 cpassword" name="cpassword">

		<button class="btn btn-success " name="savepassword">Save passsword</button>

	</form>



	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<script src="./js/script.js"></script>
</body>

</html>