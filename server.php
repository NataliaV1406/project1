<?php

class Controller
{
	private static $connect=false;
	
		function __construct(){
			if(!self::$connect){
				session_start();
				self::$connect = new mysqli("localhost", "root", "", "das17");
			}
			
		
		
		if (isset($_POST["key"])) {
			$key=$_POST["key"];
			call_user_func([$this, $key]);
			
		}

		if (isset($_POST["btn_login"])) {
			self::login();
		}
		
		if (isset($_POST["savechanges"])) {
			self::savechanges();
		}
		if (isset($_POST["opensettings"])) {
			self::opensettings();
		}
		if (isset($_POST["openchange"])) {
			self::openchange();
		}
		if (isset($_POST["openchange"])) {
			self::openchange();
		}
		if (isset($_POST["savechanges"])) {
			self::savechanges();
		}
		if (isset($_POST["savepassword"])) {
			self::savepassword();
		}
		if (isset($_POST["btn_photo"])) {
			self::add_photo();
		}
		if (isset($_POST["post_send"])) {
			self::post_send();
		}
		
		

	}
	function register()
	{
		$user = $_POST["user"];
		extract($user);                        // nova funkciya
		$errors = [];
		if (empty($name)) {
			$errors["name"] = "Enter your name";
		}
		if (empty($surname)) {
			$errors["surname"] = "Enter your surname";
		}
		if (empty($age)) {
			$errors["age"] = "Enter your age";
		} else if (filter_var($age, FILTER_VALIDATE_INT) === false) {
			$errors["age"] = "Enter your age correctly";
		}
		if (empty($gender)) {
			$errors["gender"] = "Select your gender";
		}
		if (empty($email)) {
			$errors["email"] = "Enter your e-mail";
		}
		if (empty($password)) {
			$errors["password"] = "Enter your password";
		}
		if (empty($cpassword)) {
			$errors["cpassword"] = "Confirm your password";
		} else if ($password != $cpassword) {
			$errors["cpassword"] = "Enter password correctly";
		}
		if (count($errors) > 0) {
			print json_encode($errors);
		} else {
			print("success");
			$password = password_hash($password, PASSWORD_DEFAULT);
			if ($gender == "male") {
				$photo = "images/male.jpg";
			} else {
				$photo = "images/female.jpg";
			}
			self::db->query("INSERT INTO social(name, surname, age, e_mail, password, photo, gender) values ('$name', '$surname', $age, '$email', '$password', '$photo', '$gender')");
			header("Location: login.php");
		}
	}
	function login()
	{
		$email = $_POST["email"];
		$password = $_POST["password"];
		$errors_login = [];


		if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors_login["email_login"] = "Enter your e-mail correctly";
		}
		if (empty($password) || strlen($password) < 6 || strlen($password) > 64) {
			$errors_login["password_login"] = "Enter your password";
		}


		if (count($errors_login) > 0) {
			$_SESSION["errors_login"] = $errors_login;
			header("Location: login.php");
		} else {
			$data = self::db->query("SELECT * FROM social WHERE e_mail='$email'")->fetch_all(true);
			if (empty($data)) {
				$errors_login["email_login"] = "emailov mard chka";
			} else {
				if (password_verify($password, $data[0]['password'])) {
					$_SESSION["data"] = $data[0];
					header("Location: profile.php");
				} else {
					$errors_login["password_login"] = "sxal password";
				}
			}

			if (count($errors_login) > 0) {
				$_SESSION["errors_login"] = $errors_login;
				header("Location: login.php");
			}
		}
	}
	function logout()
	{
		session_destroy();

		header("Location: login.php");
	}

	function opensettings()
	{
		header("Location: edit.php");
	}
	function openchange()
	{
		header("Location: password.php");
	}
	function savechanges()
	{
		$name = $_POST["name"];
		$surname = $_POST["surname"];
		$new_age = int $_POST["age"];
		$id_user = int $_SESSION["data"]["id"];
		$change_errors = [];
		if (empty($name)) {
			$change_errors["name"] = "write name";
		}
		if (empty($surname)) {
			$change_errors["surname"] = "write surname";
		}
		if (empty($new_age)) {
			$change_errors["age"] = "write age";
		}
		if (count($change_errors) > 0) {
			print json_encode($change_errors);
		} else {
			self::db->query("UPDATE social SET name='$name',surname='$surname',age=$new_age where id=$id_user ");

			$data = self::db->query("SELECT * FROM social WHERE id=$id_user")->fetch_all(true);
			$_SESSION["data"] = $data[0];
			header("Location: profile.php");
		}
	}
	function savepassword()
	{
		$old_password =  $_POST["opassword"];
		$new_password =  $_POST["npassword"];
		$c_password = $_POST["cpassword"];
		$id_user = $_SESSION["data"]["id"];
		$password_errors = [];
		if (empty($old_password)) {
			$password_errors["opassword"] = "write old password";
		}
		if (empty($new_password)) {
			$password_errors["npassword"] = "write new password";
		}
		if (empty($c_password)) {
			$password_errors["cpassword"] = "confirm new password";
		}



		if (count($password_errors) > 0) {
			$_SESSION["password_errors"] = $password_errors;
			header("location:password.php");
		} else {

			$old_password_confirm = $this->db->query("SELECT password FROM social WHERE id=$id_user")->fetch_all(true);
			if (password_verify($old_password, $old_password_confirm[0]['password'])) {

				if (!password_verify($new_password, $old_password_confirm[0]['password'])  && $new_password == $c_password) {
					$new_password = password_hash($new_password, PASSWORD_DEFAULT);
					$this->db->query("UPDATE social set password='$new_password' WHERE id='$id_user'");
					$data = $this->db->query("SELECT * FROM social WHERe id=$id_user")->fetch_all(true);
					$_SESSION["data"] = $data[0];
					header("Location: profile.php");
				} else {
					$password_errors["npassword"] = "nuyn e password";
				}
			} else {
				$password_errors["opassword"] = "sxal password";
			}

			if (count($password_errors) > 0) {
				session_start();
				$_SESSION["password_errors"] = $password_errors;
				header("Location: password.php");
			}
		}
	}
	function photo_show()
	{
		$id_user = $_SESSION["data"]["id"];
		$data = self::db->query("SELECT * FROM photos WHERE user_id=$id_user")->fetch_all(true);
		print json_encode($data);

	}
	function add_photo()
	{
		$id_user = $_SESSION["data"]["id"];
		$new_photo = $_FILES["add_photo"];
		$tmp = $new_photo["tmp_name"];
		$address = "images/" . time() . $new_photo["name"];

		if (!file_exists("images")) {
			mkdir("images");
		}
		move_uploaded_file($tmp, $address);
		self::db->query("INSERT INTO photos(user_id, photos) values('$id_user', '$address')");
		
		header("Location: photo.php");
	}
	function delete_photo()
	{
		$id_user = $_SESSION["data"]["id"];
		$id_photo = $_POST["id"];
		$data=self::db->query("DELETE * FROM photos WHERE id='$id_photo'");
		
		print json_encode($data);
		
	}
	function profile_photo(){
		$id_user = $_SESSION["data"]["id"];
		$id = int $_POST["id"];
		$url=$this->db->query("SELECT * FROM photos WHERE id='$id'")->fetch_all(true);
		$url=$url[0]['photos'];
		self::db->query("UPDATE social set photo='$url' WHERE id='$id_user'");
		
		$_SESSION["data"]["photo"]=$url;
		
	}
	function search(){
		$id_user = $_SESSION["data"]["id"];
		$text=$_POST["text"];
		$data=self::db->query("SELECT *, 
			(SELECT COUNT(*) FROM request WHERE from_id='$id_user' AND to_id=social.id) as my_req_count,
			(SELECT COUNT(*) FROM request WHERE from_id=social.id AND to_id='$id_user') as fr_req_count,
			(SELECT COUNT(*) FROM friend WHERE (my_id=social.id AND friend_id='$id_user') or
			(friend_id=social.id AND my_id='$id_user'))	 as friend_count
		 FROM social WHERE id != $id_user and (name LIKE '$text%' or surname LIKE '$text%') ")->fetch_all(true);
		print json_encode($data);
		
	}
	function sendrequest(){
		$to_id=$_POST["to_id"];
		$from_id=$_SESSION["data"]["id"];
        self::db->query("INSERT INTO request(from_id, to_id) values('$from_id', '$to_id')");

		
	}
	function notification(){
		$id_user = $_SESSION["data"]["id"];

        $data=self::db->query("SELECT social.id,name, surname, photo FROM social JOIN request on social.id=request.from_id WHERE request.to_id='$id_user'")->fetch_all(true);
        print json_encode($data);
	}
	function delete_request(){
		$from_id=$_POST["from_id"];
		$to_id=$_SESSION["data"]["id"];
        self::db->query("DELETE FROM request WHERE to_id='$to_id' AND from_id='$from_id");
        

		
	}
	function accept(){
		$my_id=$_SESSION["data"]["id"];
		$from_id=$_POST["from_id"];
		self::db->query("DELETE FROM request WHERE from_id='$from_id'");
		self::db->query("INSERT INTO friend(my_id, friend_id) values('$my_id', '$from_id')");
	}
	function friend_show(){
		$id=$_SESSION["data"]["id"];
		$data=self::db->query("SELECT id, name, surname, age, photo  FROM social WHERE  id in 
						(SELECT my_id FROM friend WHERE friend_id='$id'
						UNION 
						SELECT friend_id FROM friend WHERE my_id='$id')  ")->fetch_all(true);
		print json_encode($data);
	}
	function show_post(){
		$id=$_SESSION["data"]["id"];
		$data=self::db->query("SELECT post.*, social.name, social.surname, social.photo as social_photo,
			(SELECT COUNT(*) FROM likes WHERE post_id=post.id) as likes_count,
			(SELECT COUNT(*) FROM comment WHERE post_id=post.id) as comment_count,
			(SELECT COUNT(*) FROM likes WHERE post_id=post.id AND user_id='$id') as my_like
		    FROM post JOIN social on social.id=post.user_id	
			WHERE user_id='$id' or user_id in 
			(SELECT my_id FROM friend WHERE friend_id='$id'
			UNION SELECT friend_id FROM friend WHERE my_id='$id')")->fetch_all(true);
		print json_encode($data);
	}
	function post_send(){
		$id=$_SESSION["data"]["id"];
		
		$title=$_POST["title"];
		$post_text=$_POST["post_text"];
		date_default_timezone_set("Asia/Yerevan");
		$post_date = date("Y-m-d h:i:s");


		$new_photo = $_FILES["add_photo"];
		$tmp = $new_photo["tmp_name"];
		if( !empty($new_photo['name'])){
			$address = "images/" . time() . $new_photo["name"];
			move_uploaded_file($tmp, $address);
		}
		else{
			$address='';
		}



		$new_video = $_FILES["add_video"];
		$tmp2 = $new_video["tmp_name"];

		if( !empty($new_video['name'])){
			$address2 = "videos/" . time() . $new_video["name"];
			move_uploaded_file($tmp2, $address2);
		}
		else{
			$address2 ='';
		}
		self::db->query("INSERT INTO post(post, user_id, date,  title,photo,video) values('$post_text', '$id', '$post_date', '$title','$address','$address2')");
		header("Location: profile.php");

	}
	function like_post(){
		$id=$_SESSION["data"]["id"];
		$post_id=$_POST["id"];
        
		self::db->query("INSERT INTO likes ( user_id, post_id) values( '$id', '$post_id')");
       
	}
	function dislike_post(){
		$id=$_SESSION["data"]["id"];
		$post_id=$_POST["id"];
        
		self::db->query("DELETE  FROM likes WHERE post_id='$post_id' AND user_id='$id'");

	}
	
	
	function delete_post(){
		$id = $_SESSION["data"]["id"];
		$post_id = $_POST["id"];
		$data=self::db->query("DELETE  FROM post WHERE id='$post_id'");
		
		print json_encode($data);
	}

	
	function send_comment(){
		$id = $_SESSION["data"]["id"];
		$post_id = $_POST["id"];
		$post_text=real_escape_string($_POST["comment"]);
		date_default_timezone_set("Asia/Yerevan");
		$comment_date = date("Y-m-d h:i:s");

		self::db->query("INSERT INTO comment (post_id, user_id, text, date) values('$post_id', '$id', '$post_text', '$comment_date')");
	}
	function postcomment(){
		$post_id = $_POST["id"];
		$data=self::db->query("SELECT name, surname, photo,  comment.text, comment.date FROM social 
			JOIN comment on social.id=comment.user_id WHERE comment.post_id='$post_id'")->fetch_all(true);
		print json_encode($data);
	}
	function postlike(){
		$post_id = $_POST["id"];
		$data=self::db->query("SELECT name, surname, photo FROM social 
			JOIN likes on social.id=likes.user_id WHERE likes.post_id='$post_id'")->fetch_all(true);
		print json_encode($data);
	}
	function friendprofile_show(){
		$id = $_SESSION["data"]["id"];
		$friend_id = $_POST["id"];
		$data=self::db->query("SELECT * FROM social WHERE id='$friend_id'")->fetch_all(true);
		print json_encode($data);
	}
	
	function friendprofile_post(){
		$id=$_POST["id"];
		$data=self::db->query("SELECT *  FROM social JOIN post on social.id=post.user_id
		 WHERE  post.user_id = '$id'")->fetch_all(true);
		print json_encode($data);
	}
	function friend_photo(){
		$id=$_POST["id"];
		$data = self::db->query("SELECT * FROM photos WHERE user_id='$id'")->fetch_all(true);
		print json_encode($data);
	}
	function friend_friends(){
		$id=$_POST["id"];
        $my_id=$_SESSION["data"]["id"];
		$data=self::db->query("SELECT id, name, surname, age, photo  FROM social WHERE  id in 
						(SELECT my_id FROM friend WHERE friend_id='$id'
						UNION 
						SELECT friend_id FROM friend WHERE friend_id='$id')  AND id != $my_id AND id != $id ")->fetch_all(true);
		print json_encode($data);
	}
	function friend_message(){
		 $from_id=$_SESSION["data"]["id"];
		 $to_id=$_POST["id"];
		 $message_text=self::db->real_escape_string($_POST["message"]);
		date_default_timezone_set("Asia/Yerevan");
		$message_date = date("Y-m-d h:i:s");

		self::db->query("INSERT INTO messages (user_id, to_id, time, text) 
			values('$from_id', '$to_id', '$message_date', '$message_text')");
		

	}
	function friend_chat(){
		$from_id=$_SESSION["data"]["id"];
		 $to_id=$_POST["id"];
		 $data=self::db->query("SELECT * FROM social INNER 
				JOIN messages ON messages.user_id = social.id AND messages.to_id = social.id")->fetch_all(true);
		print json_encode($data);
	}
	function message_show(){
		$my_id=$_SESSION["data"]["id"];
		$friend_id=$_POST["id"];
		$data=self::db->query("SELECT messages.*, social.id as social_id,  social.name, social.surname, social.photo as social_photo,
			(SELECT COUNT(*) FROM messages WHERE to_id='$my_id' ) as message_count

			FROM messages JOIN social ON social.id = messages.user_id  
			WHERE user_id='$my_id' AND to_id='$friend_id' or user_id='$friend_id' AND to_id='$my_id'")->fetch_all(true);
		print json_encode($data);
	}
	
}
new Controller();
